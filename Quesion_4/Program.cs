﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quesion_4
{
    class Program
    {
        static void Main(string[] args)
        {
            int i;
            int sum, max;

            Console.Write("Введите минимальное численное значение: ");
            i = int.Parse(Console.ReadLine());

            Console.Write("Введите максимальное численное значение: ");
            max = int.Parse(Console.ReadLine());

            sum = 0;

            while (i <= max)
            {
                sum = sum+i;
                Console.WriteLine($"Сумма всех целых чисел = {sum}");

                i++;
            }       
             
            Console.ReadKey();
        }
    }
}
