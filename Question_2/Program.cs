﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int a, b;

            a = int.Parse(Console.ReadLine());

            while(i<=10)
            {
                b = a * i;
                Console.WriteLine($"{i}*{a}={b}");
                i++;
            }
            Console.ReadKey();
        }
    }
}
