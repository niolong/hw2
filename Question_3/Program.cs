﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Question_3
{
    class Program
    {
        static void Main(string[] args)
        {
            int i = 0;
            int a, b;

            Console.Write("Введите курс доллара: ");
            a = int.Parse(Console.ReadLine());

            while (i <= 20)
            {
                b = a * i;
                Console.WriteLine($"{i}$ = {b}руб.");
                i++;
            }

            Console.ReadKey();
        }
    }
}
